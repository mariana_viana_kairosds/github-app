import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ListUsersProxyService } from './list-users/list-users-proxy.service';
import { ListUsersComponent } from './list-users/list-users.component';
import { ListUsersService } from './list-users/list-users.service';

@NgModule({
  declarations: [
    AppComponent,
    ListUsersComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [ListUsersService, ListUsersProxyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
