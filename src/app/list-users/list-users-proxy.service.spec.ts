import { HttpClientModule } from '@angular/common/http';
import { inject, TestBed, waitForAsync } from '@angular/core/testing';
import { ListUsersProxyService } from './list-users-proxy.service';

describe('ListUsersProxyService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [ListUsersProxyService]
    });
  });

  it('should be created', inject([ListUsersProxyService], 
    (service: ListUsersProxyService) => {expect(service).toBeTruthy()}));

    it ('should get users from server', waitForAsync(() => {
      const proxy: ListUsersProxyService = TestBed.get(ListUsersProxyService);
      proxy.getUsers().subscribe(
      response => {
      expect(response.body).not.toBeNull();
      }
      );}));

});
