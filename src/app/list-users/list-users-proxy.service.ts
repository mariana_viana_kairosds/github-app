import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class ListUsersProxyService {

  constructor(private httpClient: HttpClient) { }

  getUsers(): Observable<HttpResponse<any>> {
    return this.httpClient.get<any>('https://api.github.com/users', {observe: 'response'})
  }
}
