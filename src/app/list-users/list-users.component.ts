import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ListUsersService } from './list-users.service';
import { User } from './user';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {
  
  users$!: Observable<User[]>;

  constructor(private service: ListUsersService) { }

  ngOnInit(): void {
    this.users$ = this.service.getUsers();
  }

}
